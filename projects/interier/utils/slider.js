// function setSlide(slideBlocks) {
//   for (let i = 0; i < slideBlocks.length; i++) {
//     slideBlocks[i].style.cssText = `order: ${i};`;
//   }
// }

function getMaxSHift(slideBlocks, widthBlock) {
   return (slideBlocks.length - 3) * widthBlock;
}

function getWidthShift(slideBlocks) {
  const styleBlock = getComputedStyle(slideBlocks[0]);
  const widthBlock = Number(styleBlock.width.slice(0, -2));
  const margins = styleBlock.margin.match(/\d+\.?\d*px/g);
  const marginBlockSide = Math.round(Number(margins[1].slice(0, -2)));

  return widthBlock + marginBlockSide * 2;
}

function setNextSlide(layoutSlider, widthBlock, widthShift, maxShift, btns) {
  if (Math.abs(widthShift) < maxShift) {
    widthShift -= widthBlock;
    layoutSlider.style.cssText = `transform: translateX(${widthShift}px)`;
    btns.prev.style.cssText = "display: block;";
  }
  if (Math.abs(widthShift) >= maxShift) {
    btns.next.style.cssText = "display: none;";
  }

  return widthShift;
}

function setPrevSlide(layoutSlider, widthBlock, widthShift, maxShift, btns) {
  if (widthShift < 0) {
    widthShift += widthBlock;
    layoutSlider.style.cssText = `transform: translateX(${widthShift}px)`;
    btns.next.style.cssText = "display: block;";
  }
  if (Math.abs(widthShift) < widthBlock) {
    btns.prev.style.cssText = "display: none;";
  }

  return widthShift;
}

const Slider = function(myDocument) {
  const containerSlider = myDocument.querySelector('.container-slider');
  const layoutSlider = containerSlider.querySelector('.layout-slider');
  const prevSlide = containerSlider.querySelector('.prev-slide');
  const nextSlide = containerSlider.querySelector('.next-slide');
  const slideBlocks = containerSlider.querySelectorAll('.block-slider')
  const widthShiftBlock = getWidthShift(slideBlocks);
  let widthShiftLayout = 0;
  const maxShift = getMaxSHift(slideBlocks, widthShiftBlock);
  const btns = {
    next: nextSlide,
    prev: prevSlide
  }

  prevSlide.style.cssText = "display: none";

  prevSlide.onclick = () => {
    widthShiftLayout = setPrevSlide(layoutSlider, widthShiftBlock, widthShiftLayout, maxShift, btns);
  }

  nextSlide.onclick = () => {
    widthShiftLayout = setNextSlide(layoutSlider, widthShiftBlock, widthShiftLayout, maxShift, btns);
  }
}

export default Slider;
