import Vue from 'vue'
import Router from 'vue-router'

// The meta data for your routes
const meta = require('./meta.json')

// Function to create routes
// Is default lazy but can be changed
function route (path, view, name, params, query) {
  return {
    path: path,
    meta: meta[path],
    name: name,
    params: params,
    query: query,
    component: resolve => import(`pages/${view}.vue`).then(resolve)
  }
}

Vue.use(Router)

export function createRouter () {
    const router = new Router({
      base: __dirname,
      mode: 'history',
      scrollBehavior: () => ({ y: 0 }),
      routes: [
        route('/', 'Home'),
        route('/catalog', 'Catalog', 'catalog'),
        route('/profile', 'Profile', 'profile'),
        route('/producers', 'Producers', 'producers'),
        route('/addresses', 'Addresses', 'addresses'),
        route('/designer/:id', 'Designer', 'designer'),
        route('/designer/:id/info', 'DesignerInfo', 'designerInfo'),
        route('/shares', 'Shares', 'shares'),
        route('/shares/:slug', 'Share'),
        route('/shop/:id', 'Shop', 'shop', {}, {plan: 'private'}),
        route('/shop/:id/profile', 'shop/ProfileShop', 'profileShop'),
        route('/shop/:id/projects', 'shop/ProjectsShop', 'projectsShop'),
        route('/shop/:id/gallery', 'shop/GalleryShop', 'galleryShop'),
        // Global redirect for 404
        { path: '*', redirect: '/' }
      ]
    })

    // Send a pageview to Google Analytics
    router.beforeEach((to, from, next) => {
        if (typeof ga !== 'undefined') {
            ga('set', 'page', to.path)
            ga('send', 'pageview')
        }
        next()
    })

    return router
}
